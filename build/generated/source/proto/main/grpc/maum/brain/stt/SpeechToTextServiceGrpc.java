package maum.brain.stt;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * header
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.20.0)",
    comments = "Source: stt.proto")
public final class SpeechToTextServiceGrpc {

  private SpeechToTextServiceGrpc() {}

  public static final String SERVICE_NAME = "maum.brain.stt.SpeechToTextService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<maum.brain.stt.Stt.Speech,
      maum.brain.stt.Stt.Text> getSimpleRecognizeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SimpleRecognize",
      requestType = maum.brain.stt.Stt.Speech.class,
      responseType = maum.brain.stt.Stt.Text.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<maum.brain.stt.Stt.Speech,
      maum.brain.stt.Stt.Text> getSimpleRecognizeMethod() {
    io.grpc.MethodDescriptor<maum.brain.stt.Stt.Speech, maum.brain.stt.Stt.Text> getSimpleRecognizeMethod;
    if ((getSimpleRecognizeMethod = SpeechToTextServiceGrpc.getSimpleRecognizeMethod) == null) {
      synchronized (SpeechToTextServiceGrpc.class) {
        if ((getSimpleRecognizeMethod = SpeechToTextServiceGrpc.getSimpleRecognizeMethod) == null) {
          SpeechToTextServiceGrpc.getSimpleRecognizeMethod = getSimpleRecognizeMethod = 
              io.grpc.MethodDescriptor.<maum.brain.stt.Stt.Speech, maum.brain.stt.Stt.Text>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "maum.brain.stt.SpeechToTextService", "SimpleRecognize"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  maum.brain.stt.Stt.Speech.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  maum.brain.stt.Stt.Text.getDefaultInstance()))
                  .setSchemaDescriptor(new SpeechToTextServiceMethodDescriptorSupplier("SimpleRecognize"))
                  .build();
          }
        }
     }
     return getSimpleRecognizeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<maum.brain.stt.Stt.Speech,
      maum.brain.stt.Stt.Segment> getStreamRecognizeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "StreamRecognize",
      requestType = maum.brain.stt.Stt.Speech.class,
      responseType = maum.brain.stt.Stt.Segment.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<maum.brain.stt.Stt.Speech,
      maum.brain.stt.Stt.Segment> getStreamRecognizeMethod() {
    io.grpc.MethodDescriptor<maum.brain.stt.Stt.Speech, maum.brain.stt.Stt.Segment> getStreamRecognizeMethod;
    if ((getStreamRecognizeMethod = SpeechToTextServiceGrpc.getStreamRecognizeMethod) == null) {
      synchronized (SpeechToTextServiceGrpc.class) {
        if ((getStreamRecognizeMethod = SpeechToTextServiceGrpc.getStreamRecognizeMethod) == null) {
          SpeechToTextServiceGrpc.getStreamRecognizeMethod = getStreamRecognizeMethod = 
              io.grpc.MethodDescriptor.<maum.brain.stt.Stt.Speech, maum.brain.stt.Stt.Segment>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "maum.brain.stt.SpeechToTextService", "StreamRecognize"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  maum.brain.stt.Stt.Speech.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  maum.brain.stt.Stt.Segment.getDefaultInstance()))
                  .setSchemaDescriptor(new SpeechToTextServiceMethodDescriptorSupplier("StreamRecognize"))
                  .build();
          }
        }
     }
     return getStreamRecognizeMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SpeechToTextServiceStub newStub(io.grpc.Channel channel) {
    return new SpeechToTextServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SpeechToTextServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new SpeechToTextServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SpeechToTextServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new SpeechToTextServiceFutureStub(channel);
  }

  /**
   * <pre>
   * header
   * </pre>
   */
  public static abstract class SpeechToTextServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Speech> simpleRecognize(
        io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Text> responseObserver) {
      return asyncUnimplementedStreamingCall(getSimpleRecognizeMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Speech> streamRecognize(
        io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Segment> responseObserver) {
      return asyncUnimplementedStreamingCall(getStreamRecognizeMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSimpleRecognizeMethod(),
            asyncClientStreamingCall(
              new MethodHandlers<
                maum.brain.stt.Stt.Speech,
                maum.brain.stt.Stt.Text>(
                  this, METHODID_SIMPLE_RECOGNIZE)))
          .addMethod(
            getStreamRecognizeMethod(),
            asyncBidiStreamingCall(
              new MethodHandlers<
                maum.brain.stt.Stt.Speech,
                maum.brain.stt.Stt.Segment>(
                  this, METHODID_STREAM_RECOGNIZE)))
          .build();
    }
  }

  /**
   * <pre>
   * header
   * </pre>
   */
  public static final class SpeechToTextServiceStub extends io.grpc.stub.AbstractStub<SpeechToTextServiceStub> {
    private SpeechToTextServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SpeechToTextServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SpeechToTextServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SpeechToTextServiceStub(channel, callOptions);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Speech> simpleRecognize(
        io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Text> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getSimpleRecognizeMethod(), getCallOptions()), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Speech> streamRecognize(
        io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Segment> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(getStreamRecognizeMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   * <pre>
   * header
   * </pre>
   */
  public static final class SpeechToTextServiceBlockingStub extends io.grpc.stub.AbstractStub<SpeechToTextServiceBlockingStub> {
    private SpeechToTextServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SpeechToTextServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SpeechToTextServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SpeechToTextServiceBlockingStub(channel, callOptions);
    }
  }

  /**
   * <pre>
   * header
   * </pre>
   */
  public static final class SpeechToTextServiceFutureStub extends io.grpc.stub.AbstractStub<SpeechToTextServiceFutureStub> {
    private SpeechToTextServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SpeechToTextServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SpeechToTextServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SpeechToTextServiceFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_SIMPLE_RECOGNIZE = 0;
  private static final int METHODID_STREAM_RECOGNIZE = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SpeechToTextServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SpeechToTextServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SIMPLE_RECOGNIZE:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.simpleRecognize(
              (io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Text>) responseObserver);
        case METHODID_STREAM_RECOGNIZE:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.streamRecognize(
              (io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Segment>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SpeechToTextServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SpeechToTextServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return maum.brain.stt.Stt.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SpeechToTextService");
    }
  }

  private static final class SpeechToTextServiceFileDescriptorSupplier
      extends SpeechToTextServiceBaseDescriptorSupplier {
    SpeechToTextServiceFileDescriptorSupplier() {}
  }

  private static final class SpeechToTextServiceMethodDescriptorSupplier
      extends SpeechToTextServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SpeechToTextServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SpeechToTextServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SpeechToTextServiceFileDescriptorSupplier())
              .addMethod(getSimpleRecognizeMethod())
              .addMethod(getStreamRecognizeMethod())
              .build();
        }
      }
    }
    return result;
  }
}
