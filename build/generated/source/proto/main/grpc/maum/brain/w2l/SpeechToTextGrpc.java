package maum.brain.w2l;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.20.0)",
    comments = "Source: w2l.proto")
public final class SpeechToTextGrpc {

  private SpeechToTextGrpc() {}

  public static final String SERVICE_NAME = "maum.brain.w2l.SpeechToText";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<maum.brain.w2l.W2L.Speech,
      maum.brain.w2l.W2L.Text> getRecognizeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Recognize",
      requestType = maum.brain.w2l.W2L.Speech.class,
      responseType = maum.brain.w2l.W2L.Text.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<maum.brain.w2l.W2L.Speech,
      maum.brain.w2l.W2L.Text> getRecognizeMethod() {
    io.grpc.MethodDescriptor<maum.brain.w2l.W2L.Speech, maum.brain.w2l.W2L.Text> getRecognizeMethod;
    if ((getRecognizeMethod = SpeechToTextGrpc.getRecognizeMethod) == null) {
      synchronized (SpeechToTextGrpc.class) {
        if ((getRecognizeMethod = SpeechToTextGrpc.getRecognizeMethod) == null) {
          SpeechToTextGrpc.getRecognizeMethod = getRecognizeMethod = 
              io.grpc.MethodDescriptor.<maum.brain.w2l.W2L.Speech, maum.brain.w2l.W2L.Text>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "maum.brain.w2l.SpeechToText", "Recognize"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  maum.brain.w2l.W2L.Speech.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  maum.brain.w2l.W2L.Text.getDefaultInstance()))
                  .setSchemaDescriptor(new SpeechToTextMethodDescriptorSupplier("Recognize"))
                  .build();
          }
        }
     }
     return getRecognizeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<maum.brain.w2l.W2L.Speech,
      maum.brain.w2l.W2L.TextSegment> getStreamRecognizeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "StreamRecognize",
      requestType = maum.brain.w2l.W2L.Speech.class,
      responseType = maum.brain.w2l.W2L.TextSegment.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<maum.brain.w2l.W2L.Speech,
      maum.brain.w2l.W2L.TextSegment> getStreamRecognizeMethod() {
    io.grpc.MethodDescriptor<maum.brain.w2l.W2L.Speech, maum.brain.w2l.W2L.TextSegment> getStreamRecognizeMethod;
    if ((getStreamRecognizeMethod = SpeechToTextGrpc.getStreamRecognizeMethod) == null) {
      synchronized (SpeechToTextGrpc.class) {
        if ((getStreamRecognizeMethod = SpeechToTextGrpc.getStreamRecognizeMethod) == null) {
          SpeechToTextGrpc.getStreamRecognizeMethod = getStreamRecognizeMethod = 
              io.grpc.MethodDescriptor.<maum.brain.w2l.W2L.Speech, maum.brain.w2l.W2L.TextSegment>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "maum.brain.w2l.SpeechToText", "StreamRecognize"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  maum.brain.w2l.W2L.Speech.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  maum.brain.w2l.W2L.TextSegment.getDefaultInstance()))
                  .setSchemaDescriptor(new SpeechToTextMethodDescriptorSupplier("StreamRecognize"))
                  .build();
          }
        }
     }
     return getStreamRecognizeMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SpeechToTextStub newStub(io.grpc.Channel channel) {
    return new SpeechToTextStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SpeechToTextBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new SpeechToTextBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SpeechToTextFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new SpeechToTextFutureStub(channel);
  }

  /**
   */
  public static abstract class SpeechToTextImplBase implements io.grpc.BindableService {

    /**
     */
    public io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.Speech> recognize(
        io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.Text> responseObserver) {
      return asyncUnimplementedStreamingCall(getRecognizeMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.Speech> streamRecognize(
        io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.TextSegment> responseObserver) {
      return asyncUnimplementedStreamingCall(getStreamRecognizeMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getRecognizeMethod(),
            asyncClientStreamingCall(
              new MethodHandlers<
                maum.brain.w2l.W2L.Speech,
                maum.brain.w2l.W2L.Text>(
                  this, METHODID_RECOGNIZE)))
          .addMethod(
            getStreamRecognizeMethod(),
            asyncBidiStreamingCall(
              new MethodHandlers<
                maum.brain.w2l.W2L.Speech,
                maum.brain.w2l.W2L.TextSegment>(
                  this, METHODID_STREAM_RECOGNIZE)))
          .build();
    }
  }

  /**
   */
  public static final class SpeechToTextStub extends io.grpc.stub.AbstractStub<SpeechToTextStub> {
    private SpeechToTextStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SpeechToTextStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SpeechToTextStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SpeechToTextStub(channel, callOptions);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.Speech> recognize(
        io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.Text> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getRecognizeMethod(), getCallOptions()), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.Speech> streamRecognize(
        io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.TextSegment> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(getStreamRecognizeMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   */
  public static final class SpeechToTextBlockingStub extends io.grpc.stub.AbstractStub<SpeechToTextBlockingStub> {
    private SpeechToTextBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SpeechToTextBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SpeechToTextBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SpeechToTextBlockingStub(channel, callOptions);
    }
  }

  /**
   */
  public static final class SpeechToTextFutureStub extends io.grpc.stub.AbstractStub<SpeechToTextFutureStub> {
    private SpeechToTextFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SpeechToTextFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SpeechToTextFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SpeechToTextFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_RECOGNIZE = 0;
  private static final int METHODID_STREAM_RECOGNIZE = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SpeechToTextImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SpeechToTextImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_RECOGNIZE:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.recognize(
              (io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.Text>) responseObserver);
        case METHODID_STREAM_RECOGNIZE:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.streamRecognize(
              (io.grpc.stub.StreamObserver<maum.brain.w2l.W2L.TextSegment>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SpeechToTextBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SpeechToTextBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return maum.brain.w2l.W2L.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SpeechToText");
    }
  }

  private static final class SpeechToTextFileDescriptorSupplier
      extends SpeechToTextBaseDescriptorSupplier {
    SpeechToTextFileDescriptorSupplier() {}
  }

  private static final class SpeechToTextMethodDescriptorSupplier
      extends SpeechToTextBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SpeechToTextMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SpeechToTextGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SpeechToTextFileDescriptorSupplier())
              .addMethod(getRecognizeMethod())
              .addMethod(getStreamRecognizeMethod())
              .build();
        }
      }
    }
    return result;
  }
}
