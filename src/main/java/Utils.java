import java.io.*;

public class Utils {

    public static String readTextFromFile(File file) {
        BufferedReader br = null;
        String text = null;
        try{
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            text = br.readLine();
        }catch (FileNotFoundException e) {
        }catch(IOException e){
        }

        if(br != null) {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return text;
    }
}
