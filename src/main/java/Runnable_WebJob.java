import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.Stt;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import java.net.URI;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Runnable_WebJob implements Runnable {
    int INDEX;
    String IP;
    int PORT;
    String DIR_PATH;
    int TOTAL_THREAD;
    int LOOP;

    int TIMEOUT = 10000;

    File[] mFileList;
    int mTryCount = 0;
    int mFileIdx = 0;

    Stat mStat;

    WebSocketClient mWebSocketClient = null;


    long mBeginTime = 0;
    float mDuration = 0;

    Timer mTimer = new Timer();

    public Runnable_WebJob(String ip, int port, int index, int total_thread, int loop, String dir_path, Stat stat) {
        IP = ip;
        PORT = port;
        INDEX = index;
        TOTAL_THREAD = total_thread;
        LOOP = loop;
        DIR_PATH = dir_path;
        mStat = stat;
    }

    @Override
    public void run() {

        System.out.printf("%tT {%02d} start %n", Calendar.getInstance(), INDEX);

        File dir = new File(DIR_PATH);
        mFileList = dir.listFiles();

        for(int yy = 0; yy < LOOP; yy++) {
            for (int xx = INDEX; xx < mFileList.length / 2; xx += TOTAL_THREAD) {
                mStat.try_count++;
                mFileIdx = xx;
                connect();

                while (mWebSocketClient != null) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                    }
                }

                mTryCount++;
            }
        }
    }

    @SneakyThrows
    void connect() {
        String PARAM = String.format("?answerText=%s&channel=contact_channel&language=eng&userId=BELSNAKE&biz=BP0001&v=1&service=&type=E&userText=&", "hello");

        mWebSocketClient = new WebSocketClient(new URI("wss://" + IP + ":" + PORT + "/uapi/stt/websocket/evaluationStt" + PARAM)) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                System.out.println("WebSocket.......... onOpen()");

                tryOne();
            }

            @Override
            public void onMessage(String message) {

            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                System.out.println("WebSocket.......... onClose() : " + reason);
//                mWebSocketClient = null;
            }

            @Override
            public void onError(Exception ex) {
                System.out.println("WebSocket.......... onError()");
//                mWebSocketClient = null;
            }
        };

        mWebSocketClient.connect();
    }

    void tryOne() {
        File file = mFileList[mFileIdx*2+1];
        String ext = file.getName().split("[.]")[1];

        if(ext.equalsIgnoreCase("wav") == false) {
            System.out.println("ERROR >> 파일 오류!!!!!!!!!!!!!!!!!!!!");
            return;
        }

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.printf("%tT {%02d / %03d} file = %s : file_duration = %.2f, interval = %.2f %n", Calendar.getInstance(), INDEX, mFileIdx, file.getName(), mDuration, (System.currentTimeMillis() - mBeginTime)/1000f);

                mWebSocketClient.onError(new Exception());
            }
        }, TIMEOUT);

        mBeginTime = System.currentTimeMillis();

        /* 해당 음원의 정답 텍스를 읽어 들인다. */
        String text = Utils.readTextFromFile(mFileList[mFileIdx*2]);

        /* 음원의 속성 및 데이타를 읽어 들인다. */
        FileInputStream fileInputStream = null;
        AudioInputStream audioInputStream = null;
        AudioFileFormat audioFileFormat = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(file.getAbsoluteFile());
            BufferedInputStream bufferedIn = new BufferedInputStream(audioInputStream);
            audioInputStream = new AudioInputStream(bufferedIn, audioInputStream.getFormat(), audioInputStream.getFrameLength());


            byte[] data = new byte[4096];
            int tempBytesRead = 0;
            int byteCounter = 0;
            while ((tempBytesRead = audioInputStream.read(data, 0, data.length)) != -1) {
                bos.write(data, 0, tempBytesRead);
                byteCounter += tempBytesRead;
            }
            bos.close();
            buffer = bos.toByteArray();

            audioFileFormat = new AudioFileFormat(AudioFileFormat.Type.WAVE, audioInputStream.getFormat(), (int)audioInputStream.getFrameLength());
            audioInputStream.close(); audioInputStream = null;

//                System.out.printf("%d / %d %n", buffer.length, audioFileFormat.getFormat().getFrameSize());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if(audioInputStream != null) audioInputStream.close();
            if(fileInputStream != null) fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        System.out.printf("%tT {%02d / %03d} file = %s : Frames = %d : FrameRate: %f : duration = %f : %s %n", Calendar.getInstance(), INDEX, mFileIdx, file.getName(),
//                audioFileFormat.getFrameLength(),
//                audioFileFormat.getFormat().getFrameRate(),
//                audioFileFormat.getFrameLength() / audioFileFormat.getFormat().getFrameRate(),
//                text);

        mDuration = audioFileFormat.getFrameLength() / audioFileFormat.getFormat().getFrameRate();

        /* 재생 시간에 맞게 데이타를 전송 한다. */
        try {
            int UNIT_FRAMES = 16; // 1ms 당 전송 frame 수
            long last_time = mBeginTime;
            int send_frames = 0;
//                System.out.printf("%tT {%02d / %03d} %d/%d %n", Calendar.getInstance(), INDEX, xx/2, send_frames, audioFileFormat.getFrameLength());
            while (send_frames < audioFileFormat.getFrameLength()) {
                Thread.sleep(50);
                long current_time = System.currentTimeMillis();
                int frames = ((int) (current_time - last_time)) * UNIT_FRAMES;
                if(send_frames + frames > audioFileFormat.getFrameLength()) frames = audioFileFormat.getFrameLength() - send_frames;
//                    System.out.printf("%tT {%02d / %03d} %d/%d/%d %n", Calendar.getInstance(), INDEX, xx/2, frames, send_frames, audioFileFormat.getFrameLength());


                /* ------------------------------------------------- */
                // 데이타 전송
                /* ------------------------------------------------- */
//                Stt.Speech speech = Stt.Speech.newBuilder().setBin(ByteString.copyFrom(buffer, send_frames*2, frames*2)).build();
//                mRequestObserver.onNext(speech);

                send_frames += frames;
                last_time = current_time;
//                    System.out.printf("%tT {%02d / %03d} %d/%d %n", Calendar.getInstance(), INDEX, xx/2, send_frames, audioFileFormat.getFrameLength());
            }


            for(int yy = 0; yy < 1000; yy++) {
                if(mWebSocketClient == null) break;

                Thread.sleep(50);

                /* ------------------------------------------------- */
                // 묵음 데이타 전송
                /* ------------------------------------------------- */
//                Stt.Speech speech = Stt.Speech.newBuilder().setBin(ByteString.copyFrom(buffer, (send_frames-10)*2, 10*2)).build();
//                mRequestObserver.onNext(speech);
            }

        } catch (Exception e) {
            System.out.printf("%s %n", e.getMessage());
        }

//        System.out.printf("%tT {%02d / %03d} .......... send data %n", Calendar.getInstance(), INDEX, mFileIdx);
    }


}
