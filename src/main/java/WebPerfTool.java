import java.util.ArrayList;
import java.util.Calendar;

public class WebPerfTool {
//    static final String IP = "10.122.64.58";
    static String IP = "119.207.75.41";
    static int PORT = 10080;
    static int THREAD_NUM = 1;
    static int LOOP = 2;
    static String DIR_PATH = "d:/samsung_test/wav2";
    static Stat[] mStats;

    static boolean mFlag_Running = true;

    public static void main(String[] args) {
//        if(args.length != 4) {
//            System.out.println("USAGE: java WebPerfTool ip port thr_num file_path");
//            System.out.println("");
//            System.out.println("");
//            return;
//        }
//
//        IP = args[0];
//        PORT = Integer.parseInt(args[1]);
//        THREAD_NUM = Integer.parseInt(args[2]);
//        DIR_PATH = args[3];

        ArrayList<Thread> thread_pool = new ArrayList<Thread>();

        /* 메세지 전송 쓰레드 기동 */
        mStats = new Stat[THREAD_NUM];
        for(int xx = 0; xx < THREAD_NUM; xx++) {
            mStats[xx] = new Stat();
            Thread thread = new Thread(new Runnable_WebJob(IP, PORT, xx, THREAD_NUM, LOOP, DIR_PATH, mStats[xx]));
            thread.setDaemon(true);
            thread.start();
            thread_pool.add(thread);

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /* 통계 쓰레드 기동 */
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                long begin_time = System.currentTimeMillis();
                long before_time = System.currentTimeMillis();
                Stat cur_stat = new Stat();
                Stat before_stat = new Stat();
                int before_try_count = 0;
                while(mFlag_Running) {
                    if(System.currentTimeMillis() - before_time < 1000) {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        continue;
                    }

                    cur_stat.try_count = 0;
                    cur_stat.fail_count = 0;
                    cur_stat.succ_count = 0;
                    cur_stat.file_duration = 0;
                    cur_stat.stream_duration = 0;
                    cur_stat.max_delay = -999999999;
                    cur_stat.delay = 0;
                    for(int xx = 0; xx < mStats.length; xx++) {
                        cur_stat.try_count += mStats[xx].try_count;
                        cur_stat.fail_count += mStats[xx].fail_count;
                        cur_stat.succ_count += mStats[xx].succ_count;
                        cur_stat.file_duration += mStats[xx].file_duration;
                        cur_stat.stream_duration += mStats[xx].stream_duration;
                        cur_stat.delay += mStats[xx].delay;

                        if(mStats[xx].max_delay > cur_stat.max_delay) {
//                            System.out.printf("max_interval=%.2f, mStats[%02d].max_interval=%.2f %n", cur_stat.max_interval, xx, mStats[xx].max_interval);
                            cur_stat.max_delay = mStats[xx].max_delay;
                        }
//                        System.out.printf("[%d] try =%d %n", xx, mStats[xx].try_count);
                    }

                    System.out.printf("%tT (%-4d) 시도:%-6d 실패:%-6d 성공:%-6d TPS:%-6d 재생시간:%.2f/%.2f 평균시간:%.2f/%.2f 시간차이:%.2f 최대지연:%.2f 평균지연:%.2f %n",
                            Calendar.getInstance(),
                            (System.currentTimeMillis() - begin_time)/1000,
                            cur_stat.try_count,
                            cur_stat.fail_count,
                            cur_stat.succ_count,
                            cur_stat.try_count - before_try_count,
//                            cur_stat.try_count/((System.currentTimeMillis() - begin_time)/1000),
                            cur_stat.stream_duration,
                            cur_stat.file_duration,
                            cur_stat.stream_duration/(cur_stat.try_count - THREAD_NUM),
                            cur_stat.file_duration/(cur_stat.try_count - THREAD_NUM),
                            cur_stat.stream_duration/(cur_stat.try_count - THREAD_NUM) - cur_stat.file_duration/(cur_stat.try_count - THREAD_NUM),
                            cur_stat.max_delay,
                            cur_stat.delay / (cur_stat.try_count - THREAD_NUM)
                            );

                    before_try_count = cur_stat.try_count;
                    before_time = System.currentTimeMillis();
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

        /* 메세지 전송 쓰레드 종료 대기 */
        for(int xx = 0; xx < thread_pool.size(); xx++) {
            try {
                thread_pool.get(xx).join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mFlag_Running = false;


        System.out.println("모든 작업이 완료되었습니다.");
        System.exit(0);
    }
}
