import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import maum.brain.w2l.SpeechToTextGrpc;
import maum.brain.w2l.W2L;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Runnable_CnnJob implements Runnable {
    int INDEX;
    String IP;
    int PORT;
    String DIR_PATH;
    int TOTAL_THREAD;
    int LOOP;

    int TIMEOUT = 20000;

    File[] mFileList;
    int mTryCount = 0;
    int mFileIdx = 0;

    Stat mStat;

    ManagedChannel mChannel;
    private SpeechToTextGrpc.SpeechToTextStub mAsyncStub;
    private StreamObserver<W2L.Speech> mRequestObserver;
    private ResponseObserver mResponseObserver;

    boolean mFlag_TryToComplete = false;
    long mBeginTime = 0;
    float mDuration = 0;

    Timer mTimer = new Timer();

    public Runnable_CnnJob(String ip, int port, int index, int total_thread, int loop, String dir_path, Stat stat) {
        IP = ip;
        PORT = port;
        INDEX = index;
        TOTAL_THREAD = total_thread;
        LOOP = loop;
        DIR_PATH = dir_path;
        mStat = stat;
    }

    @Override
    public void run() {

        System.out.printf("%tT {%02d} start %n", Calendar.getInstance(), INDEX);

        File dir = new File(DIR_PATH);
        mFileList = dir.listFiles();


        for(int yy = 0; yy < LOOP; yy++) {
            for (int xx = INDEX; xx < mFileList.length / 2; xx += TOTAL_THREAD) {
                mStat.try_count++;
                mFileIdx = xx;

                mBeginTime = System.currentTimeMillis();
                connect();
//                System.out.printf("connected............. interval = %.2f %n", (System.currentTimeMillis() - mBeginTime)/1000f);

//                tryOne_Realtime();
//                tryOne_Simple();
                tryOne();

                while (!mChannel.isTerminated()) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                    }
                }

                mTryCount++;
            }
        }


        while(true) {
            try {
                mChannel.awaitTermination(100, TimeUnit.MILLISECONDS);
                if(mChannel.isTerminated()) break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    void connect() {
        mFlag_TryToComplete = false;

        mChannel = ManagedChannelBuilder.forAddress(IP, PORT).usePlaintext().build();
        mAsyncStub = SpeechToTextGrpc.newStub(mChannel);

        Metadata meta = new Metadata();
        Metadata.Key<String> key1 = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> key2 = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> key3 = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);
        meta.put(key1, "eng");
        meta.put(key2, "16000");
        meta.put(key3, "wps");
        mAsyncStub = MetadataUtils.attachHeaders(mAsyncStub, meta);


        mResponseObserver = new ResponseObserver();
        mRequestObserver = mAsyncStub.streamRecognize(mResponseObserver);
    }

    void tryOne() {
        File file = mFileList[mFileIdx*2+1];
        String ext = file.getName().split("[.]")[1];

        if(ext.equalsIgnoreCase("wav") == false) {
            System.out.println("ERROR >> 파일 오류!!!!!!!!!!!!!!!!!!!!");
            return;
        }

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.printf("%tT {%02d / %03d} file = %s : file_duration = %.2f, interval = %.2f %n", Calendar.getInstance(), INDEX, mFileIdx, file.getName(), mDuration, (System.currentTimeMillis() - mBeginTime)/1000f);

                mRequestObserver.onError(new Throwable());
            }
        }, TIMEOUT);



        /* 해당 음원의 정답 텍스를 읽어 들인다. */
        String text = Utils.readTextFromFile(mFileList[mFileIdx*2]);

        /* 음원의 속성 및 데이타를 읽어 들인다. */
        FileInputStream fileInputStream = null;
        AudioInputStream audioInputStream = null;
        AudioFileFormat audioFileFormat = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(file.getAbsoluteFile());
            BufferedInputStream bufferedIn = new BufferedInputStream(audioInputStream);
            audioInputStream = new AudioInputStream(bufferedIn, audioInputStream.getFormat(), audioInputStream.getFrameLength());


            byte[] data = new byte[4096];
            int tempBytesRead = 0;
            int byteCounter = 0;
            while ((tempBytesRead = audioInputStream.read(data, 0, data.length)) != -1) {
                bos.write(data, 0, tempBytesRead);
                byteCounter += tempBytesRead;
            }
            bos.close();
            buffer = bos.toByteArray();

            audioFileFormat = new AudioFileFormat(AudioFileFormat.Type.WAVE, audioInputStream.getFormat(), (int)audioInputStream.getFrameLength());
            audioInputStream.close(); audioInputStream = null;

//                System.out.printf("%d / %d %n", buffer.length, audioFileFormat.getFormat().getFrameSize());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if(audioInputStream != null) audioInputStream.close();
            if(fileInputStream != null) fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        System.out.printf("%tT {%02d / %03d} file = %s : Frames = %d : FrameRate: %f : duration = %f : %s %n", Calendar.getInstance(), INDEX, mFileIdx, file.getName(),
//                audioFileFormat.getFrameLength(),
//                audioFileFormat.getFormat().getFrameRate(),
//                audioFileFormat.getFrameLength() / audioFileFormat.getFormat().getFrameRate(),
//                text);

        mDuration = audioFileFormat.getFrameLength() / audioFileFormat.getFormat().getFrameRate();

        /* 재생 시간에 맞게 데이타를 전송 한다. */
        try {
            int UNIT_FRAMES = 16; // 1ms 당 전송 frame 수
            long last_time = mBeginTime;
            int send_frames = 0;
//                System.out.printf("%tT {%02d / %03d} %d/%d %n", Calendar.getInstance(), INDEX, xx/2, send_frames, audioFileFormat.getFrameLength());
            while (send_frames < audioFileFormat.getFrameLength()) {
                Thread.sleep(50);
                long current_time = System.currentTimeMillis();
                int frames = ((int) (current_time - last_time)) * UNIT_FRAMES;
                if(send_frames + frames > audioFileFormat.getFrameLength()) frames = audioFileFormat.getFrameLength() - send_frames;
//                    System.out.printf("%tT {%02d / %03d} %d/%d/%d %n", Calendar.getInstance(), INDEX, xx/2, frames, send_frames, audioFileFormat.getFrameLength());

                W2L.Speech speech = W2L.Speech.newBuilder().setBin(ByteString.copyFrom(buffer, send_frames*2, frames*2)).build();
                mRequestObserver.onNext(speech);

                send_frames += frames;
                last_time = current_time;
//                    System.out.printf("%tT {%02d / %03d} %d/%d %n", Calendar.getInstance(), INDEX, xx/2, send_frames, audioFileFormat.getFrameLength());
            }


            for(int yy = 0; yy < 1000; yy++) {
                if(mChannel.isTerminated()) break;

                Thread.sleep(50);
                W2L.Speech speech = W2L.Speech.newBuilder().setBin(ByteString.copyFrom(buffer, (send_frames-10)*2, 10*2)).build();
                mRequestObserver.onNext(speech);
            }

        } catch (Exception e) {
            System.out.printf("%s %n", e.getMessage());
        }

//        System.out.printf("%tT {%02d / %03d} .......... send data %n", Calendar.getInstance(), INDEX, mFileIdx);
    }

    void tryOne_Realtime() {
        File file = mFileList[mFileIdx*2+1];
        String ext = file.getName().split("[.]")[1];

        if(ext.equalsIgnoreCase("wav") == false) {
            System.out.println("ERROR >> 파일 오류!!!!!!!!!!!!!!!!!!!!");
            return;
        }

        /* 해당 음원의 정답 텍스를 읽어 들인다. */
        String text = Utils.readTextFromFile(mFileList[mFileIdx*2]);

        /* 음원의 속성 및 데이타를 읽어 들인다. */
        FileInputStream fileInputStream = null;
        AudioInputStream audioInputStream = null;
        AudioFileFormat audioFileFormat = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(file.getAbsoluteFile());
            BufferedInputStream bufferedIn = new BufferedInputStream(audioInputStream);
            audioInputStream = new AudioInputStream(bufferedIn, audioInputStream.getFormat(), audioInputStream.getFrameLength());


            byte[] data = new byte[4096];
            int tempBytesRead = 0;
            int byteCounter = 0;
            while ((tempBytesRead = audioInputStream.read(data, 0, data.length)) != -1) {
                bos.write(data, 0, tempBytesRead);
                byteCounter += tempBytesRead;
            }
            bos.close();
            buffer = bos.toByteArray();

            audioFileFormat = new AudioFileFormat(AudioFileFormat.Type.WAVE, audioInputStream.getFormat(), (int)audioInputStream.getFrameLength());
            audioInputStream.close(); audioInputStream = null;

//                System.out.printf("%d / %d %n", buffer.length, audioFileFormat.getFormat().getFrameSize());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if(audioInputStream != null) audioInputStream.close();
            if(fileInputStream != null) fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        System.out.printf("%tT {%02d / %03d} file = %s : Frames = %d : FrameRate: %f : duration = %f : %s %n", Calendar.getInstance(), INDEX, mFileIdx, file.getName(),
//                audioFileFormat.getFrameLength(),
//                audioFileFormat.getFormat().getFrameRate(),
//                audioFileFormat.getFrameLength() / audioFileFormat.getFormat().getFrameRate(),
//                text);

        mDuration = audioFileFormat.getFrameLength() / audioFileFormat.getFormat().getFrameRate();

        /* 재생 시간에 맞게 데이타를 전송 한다. */
        mBeginTime = System.currentTimeMillis();
        try {
            int UNIT_FRAMES = 16; // 1ms 당 전송 frame 수
            long last_time = mBeginTime;
            int send_frames = 0;
//                System.out.printf("%tT {%02d / %03d} %d/%d %n", Calendar.getInstance(), INDEX, xx/2, send_frames, audioFileFormat.getFrameLength());
            while (send_frames < audioFileFormat.getFrameLength()) {
                Thread.sleep(50);
                long current_time = System.currentTimeMillis();
                int frames = ((int) (current_time - last_time)) * UNIT_FRAMES;
                if(send_frames + frames > audioFileFormat.getFrameLength()) frames = audioFileFormat.getFrameLength() - send_frames;
//                    System.out.printf("%tT {%02d / %03d} %d/%d/%d %n", Calendar.getInstance(), INDEX, xx/2, frames, send_frames, audioFileFormat.getFrameLength());

                W2L.Speech speech = W2L.Speech.newBuilder().setBin(ByteString.copyFrom(buffer, send_frames*2, frames*2)).build();
                mRequestObserver.onNext(speech);

                send_frames += frames;
                last_time = current_time;
//                    System.out.printf("%tT {%02d / %03d} %d/%d %n", Calendar.getInstance(), INDEX, xx/2, send_frames, audioFileFormat.getFrameLength());
            }

            mFlag_TryToComplete = true;
            mRequestObserver.onCompleted();

        } catch (Exception e) {
            System.out.printf("%s %n", e.getMessage());
        }

//        System.out.printf("%tT {%02d / %03d} .......... send data %n", Calendar.getInstance(), INDEX, mFileIdx);
    }

    void tryOne_Simple() {
        File file = mFileList[mFileIdx*2+1];
        String ext = file.getName().split("[.]")[1];

        if(ext.equalsIgnoreCase("wav") == false) {
            System.out.println("ERROR >> 파일 오류!!!!!!!!!!!!!!!!!!!!");
            return;
        }

        /* 해당 음원의 정답 텍스를 읽어 들인다. */
        String text = Utils.readTextFromFile(mFileList[mFileIdx*2]);

        /* 음원의 속성 및 데이타를 읽어 들인다. */
        FileInputStream fileInputStream = null;
        AudioInputStream audioInputStream = null;
        AudioFileFormat audioFileFormat = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(file.getAbsoluteFile());
            BufferedInputStream bufferedIn = new BufferedInputStream(audioInputStream);
            audioInputStream = new AudioInputStream(bufferedIn, audioInputStream.getFormat(), audioInputStream.getFrameLength());


            byte[] data = new byte[4096];
            int tempBytesRead = 0;
            int byteCounter = 0;
            while ((tempBytesRead = audioInputStream.read(data, 0, data.length)) != -1) {
                bos.write(data, 0, tempBytesRead);
                byteCounter += tempBytesRead;
            }
            bos.close();
            buffer = bos.toByteArray();

            audioFileFormat = new AudioFileFormat(AudioFileFormat.Type.WAVE, audioInputStream.getFormat(), (int)audioInputStream.getFrameLength());
            audioInputStream.close(); audioInputStream = null;

//                System.out.printf("%d / %d %n", buffer.length, audioFileFormat.getFormat().getFrameSize());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if(audioInputStream != null) audioInputStream.close();
            if(fileInputStream != null) fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        System.out.printf("%tT {%02d / %03d} file = %s : Frames = %d : FrameRate: %f : duration = %f : %s %n", Calendar.getInstance(), INDEX, idx, file.getName(),
//                audioFileFormat.getFrameLength(),
//                audioFileFormat.getFormat().getFrameRate(),
//                audioFileFormat.getFrameLength() / audioFileFormat.getFormat().getFrameRate(),
//                text);

        mDuration = audioFileFormat.getFrameLength() / audioFileFormat.getFormat().getFrameRate();

        /* 재생 시간에 맞게 데이타를 전송 한다. */
        mBeginTime = System.currentTimeMillis();
        try {

            W2L.Speech speech = W2L.Speech.newBuilder().setBin(ByteString.copyFrom(buffer)).build();
            mRequestObserver.onNext(speech);

        } catch (Exception e) {
            System.out.printf("%s %n", e.getMessage());
        }

        mFlag_TryToComplete = true;
        mRequestObserver.onCompleted();

//        System.out.printf("%tT {%02d / %03d} .......... send data %n", Calendar.getInstance(), INDEX, idx);
    }

    class ResponseObserver implements StreamObserver<W2L.TextSegment> {

        @Override
        public void onNext(W2L.TextSegment value) {
//            System.out.printf("%tT {%02d} ----> onNext() : %s %n", Calendar.getInstance(), INDEX, value.getTxt());

            if(mFlag_TryToComplete == false) {
                mRequestObserver.onCompleted();
                mFlag_TryToComplete = true;
            }
        }

        @Override
        public void onError(Throwable t) {
            System.out.printf("%tT {%02d} -------------------------------------> onError() : %s %n", Calendar.getInstance(), INDEX, t.getMessage());
            mStat.fail_count++;

            mTimer.cancel();
            mChannel.shutdownNow();
        }

        @Override
        public void onCompleted() {
//            mTimer.purge();

            mStat.succ_count++;

            mStat.stream_duration += (System.currentTimeMillis() - mBeginTime)/1000f;
            mStat.file_duration += mDuration;
            if((System.currentTimeMillis() - mBeginTime)/1000f - mDuration > mStat.max_delay) mStat.max_delay = (System.currentTimeMillis() - mBeginTime)/1000f - mDuration;
            mStat.delay += (System.currentTimeMillis() - mBeginTime)/1000f - mDuration;

//            System.out.printf("%tT {%02d / %03d}  -------------------------------------> onCompleted() : interval=%.2f %n", Calendar.getInstance(), INDEX, mFileIdx,
//                    (System.currentTimeMillis() - mBeginTime)/1000f);

            mTimer.cancel();
            mChannel.shutdownNow();
        }
    }
}
